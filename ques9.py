import math


class Circle:
    def __init__(self, r):
        self.r = r

    def circum(self):
        a = (2 * (math.pi) * self.r)
        print("Circumference of the circle is", a)

    def area(self):
        b = ((math.pi) * self.r * self.r)
        print("Area of the circle is", b)


c1 = Circle(3)
c1.circum()
c1.area()

