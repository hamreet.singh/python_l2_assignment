import re
email="From abc.xyz@pqr.com Mon Dec 29 01:12:15 2016"

email_id = re.findall('\S+@\S+', email)
print("Email id: ",email_id)

domain_name = re.findall('@\S+', email)
print("Domain name: ",domain_name)

time = re.findall('\S+:\S+:\S+', email)
print("Time: ", time)