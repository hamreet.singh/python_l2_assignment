class First:
    def method1(self):
        print("Class First")


class Second(First):
    def method1(self):
        print("Class Second")


class Third(First):
    def method1(self):
        print("Class Third")


class Fourth(Second, Third):
    def method1(self):
        print("Class Fourth")


f = Fourth()
f.method1()
print(Fourth.mro())
