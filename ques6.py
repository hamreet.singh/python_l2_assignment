class Matrix:

    def __init__(self, a, b, c, d):
        self.data1 = [a, b]
        self.data2 = [c, d]

    def __add__(self, other):
        return Matrix(self.data1[0] + other.data1[0],
                      self.data1[0] + other.data1[1],
                      self.data2[0] + other.data2[0],
                      self.data2[0] + other.data2[1])


p = Matrix(1, 2, 3, 4)
q = Matrix(5, 6, 7, 8)
r = p + q
print(r)
